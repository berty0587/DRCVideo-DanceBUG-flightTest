<?php
require_once './helper/conn.php';
require_once './helper/jwt.php';

function view($id)
{
    //check if request token JWT token success
    $auth = 'Authorization';
    $key = array_key_exists($auth, userGetheaders());
    $errMessage = $status = ''; //set variable of status and error message null


    if ($key) // if token exist
    {
        //search this id user if exist
        $db = Myconnct::getDbinstane();
        $sql = "select * from users where id = (?) limit 1";
        $stmt = $db->getDbconn()->prepare($sql);
        $stmt->bind_param("d", $id);
        $stmt->execute();
        $res = $stmt->get_result();
        //$select = "select * from users where id = '".$id."' ";
        //$result = mysqli_query(db(), $select);
        if ($row = $res->fetch_assoc())
        {
            //if this id user exist, make JWT token from front end by header
            $header = userGetheaders()['Authorization'];
            $array = explode(" ", $header);
            $name = $array[0];
            $tokenHeader = $array[1];

            //make JWT token from databbase
            $payload = array('sub'=>$name,'name'=>$row['email'],'iat'=>1516239022);
            $jwt = new Jwt();
            $tokenData = $jwt->getToken($payload); //token from back end by database


            //compare front end JWT token == back end JWT token
            if ($tokenData == $tokenHeader)
            {
                //JWT tokens equaled, then make user data to array and return to front end
                $res = array(
                    'name' => $row['name'],
                    'email' =>$row['email'],
                    'birthday' => $row['birthday'],
                    'status' => '200',
                );

                return $res;
            }
            else
            {
                // if JWT tokens not equaled, then return error message
                $status = statusCode::INTERNAL;
                $errMessage = "tokens are not matched";
            }

            $stmt->close(); //close db

        }
        else // this id user not exist in DB
        {
            $status = statusCode::NOTFOUND;
            $errMessage = "this user does not exist";
        }

    }
    else
    {
        //front end JWT token is empty
        $status = statusCode::NOTFOUND;
        $errMessage = "JWT token could not empty";
    }

    $res = array (
        'status' => $status,
        'errMessage' => $errMessage,
    );

    return $res;





}