<?php
require_once './helper/conn.php';
require_once './helper/jwt.php';



// verify function for verify email
function verify()
{
    $verifyToken = Flight::request()->query['verifyToken'];
    $status = $errMessage = '';//set variable of status and error message null

    if (empty($verifyToken)) // check verifyToken if empty
    {
        //empty, then return error message
        $status = statusCode::NOTFOUND;
        $errMessage = "token is empty";
    }
    else
    {
        //verify token is exist, select this verify token's user if exist
        $db = Myconnct::getDbinstane();
        $sql = "select * from users where verify_token = (?) limit 1";
        $stmt = $db->getDbconn()->prepare($sql);
        $stmt->bind_param("s", $verifyToken);
        $stmt->execute();
        $res = $stmt->get_result();
        if ($row = $res->fetch_assoc())
        {
            // user exist, update this user's verified status to 1, means already verified
            $id = $row['id'];
            $email = $row['email'];
            $name = $row['name'];
            $birthday = $row["birthday"];

            $db = Myconnct::getDbinstane();
            $update = "update users set verified = 1 where  verify_token = ?";
            $stmt = $db->getDbconn()->prepare($update);
            $stmt->bind_param("s", $verifyToken);

            if ($stmt->execute())
            {
                //make JWT token
                $payload = array('sub'=>'Flight','name'=>$row['email'],'iat'=>1516239022);
                $jwt = new Jwt;
                $tokenData = $jwt->getToken($payload);

                //make user's array and return to front end
                $res = array(
                    'status' => '200',
                    'id' => $id,
                    'email' => $email,
                    'name' => $name,
                    'birthday' => $birthday,
                    'token' => $tokenData,

                );

                return $res;



            }
            else // update failed
            {
                $status = statusCode::INTERNAL;
                $errMessage = "update failed";
            }

            $stmt->close();
        }
        else // this verifyToken's user not exist
        {
            $status = statusCode::NOTFOUND;
            $errMessage = "this user does not exist";
        }

    }

    $res = array(
        'status' => $status,
        'errMessage' => $errMessage,
    );


    return $res;
}