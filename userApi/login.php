<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once './helper/conn.php';
require_once './helper/jwt.php';
require_once './helper/phpmailer/src/PHPMailer.php';
require_once './helper/phpmailer/src/SMTP.php';
require_once './helper/phpmailer/src/Exception.php';


function login()
{
    $status = $errMessage = ''; //set variable of status and error message null
    $email = Flight::request()->data['email'];
    $pass = Flight::request()->data['pass'];


    if (empty($email)) //check email if empty
    {
        $errMessage =  "Email is empty, please input it";
        $status = statusCode::NOTFOUND;
    }
    elseif (empty($pass))// check password if empty
    {
        $errMessage = "pass is empty, please input it!";
        $status = statusCode::NOTFOUND;
    }
    else // password and email are not empty, then select user
    {
        $db = Myconnct::getDbinstane();
        $sql = "select * from users where email = (?) limit 1";
        $stmt = $db->getDbconn()->prepare($sql);
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $res = $stmt->get_result();
        if ($row = $res->fetch_assoc()) // this email user exist
        {
            // check user's password if match
            if (password_verify($pass, $row["password"])) //if passwords match
            {
                //check email if confirmed
                if (($row['verified']) == 0) //email does not confirm, return verify token and message
                {

                    $mail = new PHPMailer(true);

                    try {
                        //server configuration
                        $mail->CharSet = sendEmailConst::CHARSET;                     //set email charset
                        $mail->SMTPDebug = 0;                        // set debug input false
                        $mail->isSMTP();                             // use SMTP
                        $mail->Host = sendEmailConst::SMTPSERVER;                // SMTP server
                        $mail->SMTPAuth = true;                      // allow STMP auth
                        $mail->Username = sendEmailConst::USERNAME;                // SMTP username = email account
                        $mail->Password = sendEmailConst::PASSWORD;             // SMTP password
                        $mail->SMTPSecure = sendEmailConst::SMPTSECURE;                    // allow TLS or ssl protocol
                        $mail->Port = 465;                            // server port: 25 or 465, gmail use port 465

                        $mail->setFrom(sendEmailConst::USERNAME, sendEmailConst::FROMUSER);  //sender's email address and name
                        $mail->addAddress($row['email'], $row['name']);  // to email address and name
                        $mail->addReplyTo(sendEmailConst::USERNAME, sendEmailConst::FROMUSER); //replay to email address, usually same as sender

                        //Content
                        $mail->isHTML(true);                                  // if use HTML format
                        $mail->Subject = sendEmailConst::TITLE ;
                        $mail->Body    = '<h1><a href=www.freeshow.download/vm/?verifyToken='.$row['verify_token'].'>confirm</a> Please</h1>'. date('Y-m-d H:i:s');
                        $mail->AltBody = 'www.freeshow.donwload/vm'.$row['verify_token'];

                        $mail->send();

                        $errEmail = 'email send success!';
                    } catch (Exception $e) {
                        $errEmail =  'email send failed: '.$mail->ErrorInfo;
                    }
                    $status = statusCode::CONTINUES;
                    $errMessage = 'email need confirm!';
                    $res = array(
                        'status' => $status,
                        'errMessage' => $errMessage,
                        'verify_token' => $row['verify_token'],
                        'errEmail' => $errEmail
                    );

                }
                else // email confirmed and return success and token of JWT
                {
                    $payload=array('sub'=>'Flight','name'=>$row['email'],'iat'=>1516239022);
                    $jwt=new Jwt;
                    $token=$jwt->getToken($payload);
                    $status = statusCode::SUCCESS;
                    $res = array(
                        'id' => $row['id'],
                        'email' =>$row['email'],
                        'birthday' => $row['birthday'],
                        'name' => $row['name'],
                        'token' => $token,
                        'status' => $status,
                    );
                }

                $stmt->close(); //close database



            }
            else // password and email do not match
            {
                $errMessage = 'password and email not match';
                $status = statusCode::INTERNAL;
            }


        }else // email address not in database
        {
            $status = statusCode::NOTFOUND;
            $errMessage = 'email not exist';
        }



    }

    if ($status <> statusCode::SUCCESS && $status <> statusCode::CONTINUES) // error status, and return error message and status
    {
        $res = array(
            'errMessage' => $errMessage,
            'status' => $status
        );
    }




    return $res;

}