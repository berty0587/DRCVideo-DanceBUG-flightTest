<?php
require_once './helper/conn.php';
require_once './helper/jwt.php';

function update($id)
{
    $name = Flight::request()->data['name'];
    $pass = Flight::request()->data['pass'];
    $pass2 = Flight::request()->data['pass2'];
    $yyyy = Flight::request()->data['YYYY'];
    $mm = Flight::request()->data['MM'];
    $dd = Flight::request()->data['DD'];
    $errMessage  = $status  = ''; //set variable of status and error message null

    // format request data from front end user
    $name = test_input($name);
    $pass = test_input($pass);
    $pass2 = test_input($pass2);

    //check if request token JWT token success
    $auth = 'Authorization';
    $key = array_key_exists($auth, userGetheaders());

    //start check year, month and day if number
    if (!is_numeric($yyyy) or !is_numeric($mm) or !is_numeric($dd))
    {
        $errMessage = "Year or Month or Day should be digital";
        $status = '500';
    }
    else
    {
        $birthday = $yyyy.'-'.$mm.'-'.$dd; // format birthday date
    }

    if ($key)  // if token exist
    {

        //start check data
        if (empty($name))
        {
            $errMessage = 'Name is required!';
            $status = statusCode::NOTFOUND;
        }
        else if (strlen($name) <= 2 || strlen($name) >= 20)
        {
            $errMessage = "Name should between 3- 20";
            $status = statusCode::INTERNAL;
        }
        else if (!preg_match("/^[a-zA-Z ]*$/",$name))
        {
            $errMessage = "Name need letters and white space allowed";
            $status = statusCode::INTERNAL;
        }else if ((empty($pass)) && empty($pass2))
        {
            $status = statusCode::CONTINUES;
        }
        else if (empty($pass) || empty($pass2))
        {
            $status = statusCode::NOTFOUND;
            $errMessage = "One of passwords is empty ";
        }
        else if (strlen($pass) <= 5 || strlen($pass) >= 12)
        {
            $errMessage = "pass should between 6- 12";
            $status = statusCode::INTERNAL;
        }
        else if (!preg_match("/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,15}$/",$pass))
        {
            $errMessage = "Password need letters and digital allowed";
            $status = statusCode::INTERNAL;

        }
        else if ($pass <> $pass2)
        {
            $errMessage = "Two passwords are not match";
            $status = statusCode::INTERNAL;
        }
        //end of check data
        if (($status == statusCode::INTERNAL) || ($status == statusCode::NOTFOUND))// error status  make error array
        {
            $res = array(
                'status' => $status,
                'errMessage' => $errMessage,
            );

            return $res;
        }
        else if ($status == statusCode::CONTINUES)
        {
            $db = Myconnct::getDbinstane();
            $update = "update users set name = ?, birthday = ? where id = ?";
            $stmt = $db->getDbconn()->prepare($update);
            $stmt->bind_param("ssi",$name, $birthday, $id);
            if ($stmt->execute())
            {
                // update success and return message
                $res = array(
                    'status' => statusCode::SUCCESS,
                    'name' => $name,
                    'errMessage' => "update success!",
                );
            }else
            {
                // update failed and return message
                $res = array(
                    'status' => statusCode::INTERNAL,
                    'errMessage' => 'data update failed',
                );
            }
            $stmt->close(); // close db
        }
        else//valid data success, insert db
        {
            $db = Myconnct::getDbinstane();
            $update = "update users set name = ?, birthday = ?, password = ? where id = ?";
            $pass = password_hash($pass, PASSWORD_DEFAULT); // encryption user's password
            $stmt = $db->getDbconn()->prepare($update);
            $stmt->bind_param("sssi",$name, $birthday, $pass, $id);
            if ($stmt->execute())
            {
                // update success and return message
                $res = array(
                    'status' => statusCode::SUCCESS,
                    'name' => $name,
                    'errMessage' => "update success!",
                );
            }else
            {
                // update failed and return message
                $res = array(
                    'status' => statusCode::INTERNAL,
                    'errMessage' => 'data update failed',
                );
            }
            $stmt->close(); // close db
        }

        return $res;

    }
    else // token empty
    {
        $status = statusCode::NOTFOUND;
        $errMessage = "token could not empty";
    }

    // make error message array
    $res = array(
        'errMessage' => $errMessage,
        'status' => $status
    );

    return $res;




}