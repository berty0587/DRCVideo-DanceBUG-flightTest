<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once './helper/conn.php';
require_once './helper/jwt.php';
require_once './helper/phpmailer/src/PHPMailer.php';
require_once './helper/phpmailer/src/SMTP.php';
require_once './helper/phpmailer/src/Exception.php';


//start restAPI of register
function reg()
{
    $name = Flight::request()->data['name'];
    $pass = Flight::request()->data['pass'];
    $pass2 = Flight::request()->data['pass2'];
    $email = Flight::request()->data['email'];
    $yyyy = Flight::request()->data['YYYY'];
    $mm = Flight::request()->data['MM'];
    $dd = Flight::request()->data['DD'];


    $errMessage  = $status = ''; //set variable of status and error message null
    // format request data from front end user
    $name = test_input($name);
    $pass = test_input($pass);
    $pass2 = test_input($pass2);
    $email = test_input($email);


    //start check year, month and day if number
    if (!is_numeric($yyyy) or !is_numeric($mm) or !is_numeric($dd))
    {
        $errMessage = "Year or Month or Day should be digital";
        $status = statusCode::INTERNAL;
    }
    else
    {
        $birthday = $yyyy.'-'.$mm.'-'.$dd; // format birthday date
    }

    //start check $name valid
    if (empty($name))
    {
        $errMessage = 'Name is required!';
        $status = statusCode::NOTFOUND;
    }
    else if (strlen($name) <= 2 || strlen($name) >= 20)
    {
        $errMessage = "Name should between 3- 20";
        $status = statusCode::INTERNAL;
    }
    else if (!preg_match("/^[a-zA-Z ]*$/",$name))
    {
        $errMessage = "Name need letters and white space allowed";
        $status = statusCode::INTERNAL;

    }
    //end check name

    //start check  passwords valid
    if (empty($pass) || empty($pass2))
    {
        $errMessage = 'Password is required!';
        $status = statusCode::NOTFOUND;
    }
    else if (strlen($pass) <= 5 || strlen($pass) >= 12)
    {
        $errMessage = "pass should between 6- 12";
        $status = statusCode::INTERNAL;
    }
    else if (!preg_match("/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,15}$/",$pass))
    {
        $errMessage = "Password need letters and digital allowed";
        $status = statusCode::INTERNAL;

    }
    else if ($pass <> $pass2)
    {
        $errMessage = "Two passwords are not match";
        $status = statusCode::INTERNAL;
    }
    //end of check passwords


    //start check  email valid
    if (empty($email))
    {
        $errMessage = "Email is required";
        $status = statusCode::NOTFOUND;
    }
    else if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
    {
        $errMessage = "Invalid email format";
        $status = statusCode::INTERNAL;
    }
    else
    {
        $db = Myconnct::getDbinstane();
        $sql = "select * from users where email = (?) limit 1";
        $stmt = $db->getDbconn()->prepare($sql);
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $res = $stmt->get_result();
        if ($row = $res->fetch_assoc())
        {
            // user exist, return error
            $errMessage = "Email address has already used!";
            $status = statusCode::INTERNAL;
        }
        $stmt->close(); // close db

    }
    // end of check email

    if (($status == statusCode::NOTFOUND) || ($status == statusCode::INTERNAL))//status = '-1' means error array
    {
        $res = array(
            'status' => $status,
            'errMessage' => $errMessage,
        );

        return $res;
    }
    else //valid data success, insert db
    {

        $pass = password_hash($pass,PASSWORD_DEFAULT);
        $verifyToken = str_random(16);
        $db = Myconnct::getDbinstane();
        $query = "insert into users (name, password, email, birthday, verify_token) value (?, ?, ?, ?, ?)";
        $stmt = $db->getDbconn()->prepare($query);
        $stmt->bind_param("sssss",$name, $pass, $email, $birthday, $verifyToken);
        $stmt->execute();
        if ($stmt->affected_rows)

        {
            // if insert success, then make JWT token and return it and data from db
            $payload=array('sub'=>'Flight','name'=>$email,'iat'=>1516239022);
            $jwt = new Jwt();
            $token = $jwt->getToken($payload);

            //return $verifyToken;

            $mail = new PHPMailer(true);

            try {
                //服务器配置
                $mail->CharSet = sendEmailConst::CHARSET;                     //set email charset
                $mail->SMTPDebug = 0;                        // set debug input false
                $mail->isSMTP();                             // use SMTP
                $mail->Host = sendEmailConst::SMTPSERVER;                // SMTP server
                $mail->SMTPAuth = true;                      // allow STMP auth
                $mail->Username = sendEmailConst::USERNAME;                // SMTP username = email account
                $mail->Password = sendEmailConst::PASSWORD;             // SMTP password
                $mail->SMTPSecure = sendEmailConst::SMPTSECURE;                    // allow TLS or ssl protocol
                $mail->Port = 465;                            // server port: 25 or 465, gmail use port 465

                $mail->setFrom(sendEmailConst::USERNAME, sendEmailConst::FROMUSER);  //sender's email address and name
                $mail->addAddress($email, $name);  // to email address and name
                $mail->addReplyTo(sendEmailConst::USERNAME, sendEmailConst::FROMUSER); //replay to email address, usually same as sender

                //Content
                $mail->isHTML(true);                                  // if use HTML format
                $mail->Subject = sendEmailConst::TITLE;
                $mail->Body    = '<h1><a href=www.freeshow.download/vm/?verifyToken='.$verifyToken.'>confirm</a> Please</h1>'. date('Y-m-d H:i:s');
                $mail->AltBody = 'www.freeshow.donwload/vm/?verifyToken='.$verifyToken;

                $mail->send();

                $errEmail = 'email send success!';
            } catch (Exception $e) {
                $errEmail =  'email send failed:'.$mail->ErrorInfo;
            }

            $errMessage = 'email need confirm!';

            $res = array(
                'status' => statusCode::SUCCESS,
                'verifyToken' => $verifyToken,
                'token' => $token,
                'errEmail' => $errEmail,
                'errMessage' => $errMessage,
            );
            return $res;
        }
        else
        {
            // insert failed
            $res = array(
                'status' => statusCode::INTERNAL,
                'errMessage' => 'data insert failed',
            );
            return $res;
        }
        $stmt->close(); // close db

    }
}