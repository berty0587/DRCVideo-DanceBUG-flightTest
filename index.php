<?php
require_once 'flight/Flight.php';
require_once 'flight/net/Response.php';
require_once 'flight/net/Request.php';
require_once 'userApi/login.php';
require_once 'userApi/update.php';
require_once 'userApi/register.php';
require_once 'userApi/view.php';
require_once 'userApi/verify.php';

//route for login view
Flight::route('/', function(){
    Flight::render('login');

});

//route for register a=API
Flight::route('POST /reg/', function (){
    $res = reg();
    Flight::json($res);
});


//route for verify view
Flight::route('/vm', function(){
    Flight::render('vm');

});


//route for register view
Flight::route('GET /reg', function(){
    Flight::render('register');

});


//route for verify email API
Flight::route('GET /verify/', function (){
    $res = verify();
    Flight::json($res);
});

//route for login API
Flight::route('POST /login/', function (){
    $res = login();
    Flight::json($res);
});


//route for update API
Flight::route('POST /update/@id', function ($id){
    $res = update($id);
    Flight::json($res);
});

//route for update view
Flight::route('GET /update', function(){
    Flight::render('update');

});

//route for view user API
Flight::route('GET /view/@id', function ($id){
    $res = view($id);
    Flight::json($res);

});


//route for view user view
Flight::route('/view', function(){
    Flight::render('view');

});

Flight::start();
